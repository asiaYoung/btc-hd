import * as ecc from 'tiny-secp256k1';
import {BIP32Factory} from 'bip32';
import {payments} from 'bitcoinjs-lib';
/*
助记词: run tornado empower copy valve pudding average crash only abuse equal ramp
种子: 526aa7fc11f711a0c3aa9ed7c29f43707d64820454f64f5efdaf62887ce5129480872f5e94bb18ea0e2ee59b6d6621cfef5432b5952f890b848ab6f9d294dcfe
私钥: f628f595a8f74c8252d38b0431949bd07151977c89f521f0484ece8bef6d6a07
公钥: 0231854cc1a711aa4ebbd7cc5861c5242dff3cf4e8af511852a2ade7461320b567
chainCode: bb96dc4dee05d60ade23f793f8c7023664c0c6514cea71871c09a11d118ddd6d
BTC XPub:xpub6Eci7xo7tgtiKDFMyaMvuZaL2KuyryP2fRqdYeA2yP67hknNun9TfG8Uvz1A6W6ECNdoZdbApZmiNZACogYuZovkmPaEgMN5Km437FCXTfM

*/
const xpub = ''
const publicKey = Buffer.from('029deaca529cd7e48452f275c5f4e374d5c482da7887a9bf48a12fd9827f7d5c79', 'hex');
const chainCode = 'bb96dc4dee05d60ade23f793f8c7023664c0c6514cea71871c09a11d118ddd6d';
async function accountByXpubOfP2pkh(index){
    let node = BIP32Factory(ecc).fromBase58(xpub).derive(index);
    let address = payments.p2pkh({ pubkey: node.publicKey }).address
    console.log(`INDEX-ACCOUNT:${index}-${address}`);
}

async function accountByPubKeyOfP2pkh(index = 0){
    let node = BIP32Factory(ecc).fromPublicKey(publicKey, Buffer.from(chainCode, 'hex')).derive(index);
    let address = payments.p2pkh({ pubkey: node.publicKey }).address
    console.log(`INDEX-ACCOUNT:${index}-${address}`);
}

async function accountByPubKeyOfP2sh(){
    const { address } = payments.p2sh({
        redeem: payments.p2ms({ m: 1, pubkeys: [publicKey] }),
      });
    console.log(`P2sh ACCOUNT:${address}`);
}

// accountByXpubOfP2pkh(0);
// accountByPubKeyOfP2pkh(0);
accountByPubKeyOfP2sh();