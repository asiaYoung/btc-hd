
import bip39 from 'bip39';
import * as ecc from 'tiny-secp256k1';
import {BIP32Factory} from 'bip32';
async function run(){

    const mnemonic = bip39.generateMnemonic();
    console.log('助记词:', mnemonic);

    const seed = await bip39.mnemonicToSeed(mnemonic);
    console.log('种子:',seed.toString('hex'));

    const node = BIP32Factory(ecc).fromSeed(seed);
    console.log('私钥:',node.privateKey.toString('hex'));

    console.log('公钥:',node.publicKey.toString('hex'));
    console.log('chainCode:',node.chainCode.toString('hex'));
    const childBtcParent = node.derivePath("m/44'/0'/0'/0");
    
    console.log("BTC XPub:" + childBtcParent.neutered().toBase58());
}
run();